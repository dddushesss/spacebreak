﻿using System;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "Data", menuName = "Custom/Data", order = 0)]
    public class Data : ScriptableObject
    {
        [SerializeField]
        private string _uiDataPath;
        private UIData _uiData;

        public UIData UIData
        {
            get
            {
                if (_uiData == null)
                {
                    _uiData = Load<UIData>("Data/" + _uiDataPath);
                }

                return _uiData;
            }
        }
        
        
        private T Load<T>(string resourcesPath) where T : Object =>
            Resources.Load<T>(Path.ChangeExtension(resourcesPath, null));
    }
}